# 4. Volumes persistants avec K8



------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______


# Objectifs

Nous allons explorer les volumes persistants dans Kubernetes. Voici un aperçu rapide de ce que nous allons aborder dans cette leçon :

1. **Volumes persistants**
2. **Classes de stockage**
3. **Requêtes de volumes persistants (Persistent Volume Claims)**
4. **Utilisation des requêtes de volumes persistants dans un pod**
5. **Redimensionnement des requêtes de volumes persistants**

Nous ferons une démonstration pratique de ces concepts, mais nous la garderons pour la prochaine vidéo afin de ne pas alourdir celle-ci. Nous allons d'abord parler des concepts, puis nous ferons une démonstration pratique dans la vidéo suivante.

# Volumes persistants

Les volumes persistants sont des objets Kubernetes qui vous permettent de traiter le stockage comme une ressource abstraite consommée par des pods, de la même manière que Kubernetes traite déjà les ressources de calcul telles que la mémoire et le CPU. Avec les volumes traditionnels que nous avons créés, nous avons défini toutes les informations directement dans la spécification du pod. Les volumes persistants nous permettent d'externaliser ces informations dans un objet séparé.

Un objet volume persistant inclut un ensemble d'attributs décrivant la ressource de stockage sous-jacente utilisée pour stocker les données. Ces ressources de stockage peuvent varier : disques, stockage en cloud, etc. L'exemple YAML ci-dessous montre un volume persistant avec des attributs spécifiques sous la section `spec`.

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: my-pv
spec:
  storageClassName: localdisk
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/var/output"
```

Un des attributs importants est `storageClassName`, qui fait référence à une classe de stockage.

# Classes de stockage

Les classes de stockage permettent aux administrateurs Kubernetes de spécifier différents types de services de stockage disponibles sur leur plateforme. Voici un exemple de définition YAML pour une classe de stockage :

```yaml
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: localdisk
provisioner: kubernetes.io/no-provisioner
```

Une classe de stockage peut être utilisée pour désigner des ressources de stockage de différentes performances et coûts. Par exemple, une classe de stockage `slow` pour les ressources de stockage peu performantes mais peu coûteuses, et une classe de stockage `fast` pour les ressources de stockage hautement performantes mais coûteuses.

Exemple : 

```yaml
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: slow #<-- Définition du nom de la Class de stockage 
provisioner: kubernetes.io/no-provisioner
```

```yaml
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: fast #<-- Définition du nom de la Class de stockage 
provisioner: kubernetes.io/no-provisioner
```

La propriété `allowVolumeExpansion` détermine si une classe de stockage permet de redimensionner les volumes après leur création. Si cette propriété n'est pas définie à `true`, tenter de redimensionner un volume générera une erreur.

Exemple : 

```yaml
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: fast #<-- Définition du nom de la Class de stockage 
provisioner: kubernetes.io/no-provisioner
allowVolumeExpansion: true  #<-- Autorise le redimensionnement du volume après création
```


# Politiques de récupération

La politique de récupération d'un PersistentVolume détermine comment les ressources de stockage peuvent être réutilisées lorsque les PersistentVolumeClaims associés sont supprimés.

- **Retain (Conserver)** : Conserve toutes les données. Cela nécessite qu'un administrateur nettoie manuellement les données et prépare la ressource de stockage pour une réutilisation.

- **Delete (Supprimer)** : Supprime automatiquement la ressource de stockage sous-jacente (fonctionne uniquement pour les ressources de stockage cloud).

- **Recycle (Recycler)** : Supprime automatiquement toutes les données de la ressource de stockage sous-jacente, permettant ainsi au PersistentVolume d'être réutilisé.


```yaml
kind: PersistentVolume
apiVersion: v1
metadata:
  name: my-pv
spec:
  storageClassName: localdisk
  persistentVolumeReclaimPolicy: Recycle #<-- 
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: /etc/output
```

Afin d'utiliser un provisionnement dynamique au lieu de l'anciene méthode on peut définir : 

1. StorageClass

```yaml
kind: StorageClass
apiVersion: storage.k8s.io/v1
metadata:
  name: localdisk
provisioner: kubernetes.io/no-provisioner
volumeBindingMode: WaitForFirstConsumer
```

2. PersistentVolumeClaim

```yaml
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: my-pvc
spec:
  storageClassName: localdisk
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
```

3. Le manifest complet serais : 

```yaml
# StorageClass definition
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: localdisk
provisioner: kubernetes.io/no-provisioner
volumeBindingMode: WaitForFirstConsumer

# PersistentVolumeClaim definition
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: my-pvc
spec:
  storageClassName: localdisk
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
```

# Requêtes de volumes persistants (Persistent Volume Claims)

Une requête de volume persistant (PVC) représente une demande de l'utilisateur pour utiliser des ressources de stockage. Elle définit un ensemble d'attributs similaires à ceux d'un volume persistant. Lorsqu'une PVC est créée, elle cherche un volume persistant capable de répondre aux critères demandés et, si elle en trouve un, elle s'y lie automatiquement.

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: my-pvc
spec:
  storageClassName: localdisk
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 100Mi
```

# Utilisation des requêtes de volumes persistants dans un pod

Une fois qu'une PVC est liée à un volume persistant, elle peut être utilisée dans un pod comme n'importe quel autre volume.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  containers:
  - name: my-container
    image: busybox
#__________________
    volumeMounts: #<-- Début de la déclaration du PV, 
    - mountPath: "/data"
      name: my-volume
  volumes:
  - name: my-volume
    persistentVolumeClaim:
      claimName: my-pvc #<-- Fin de la déclaration du PV, 
#__________________
```

# Redimensionnement des requêtes de volumes persistants

Vous pouvez redimensionner une PVC sans interrompre une application en cours d'utilisation. Il suffit de modifier la ressource PVC et d'augmenter la valeur de stockage. Cependant, cela nécessite que la classe de stockage ait la propriété `allowVolumeExpansion` définie à `true`.


```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: my-pvc
spec:
  storageClassName: localdisk
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 200Mi #<-- Augmentation de la capacité du PVC à chaud
```

# Récapitulatif

Dans cette leçon, nous avons parlé des volumes persistants, des classes de stockage, des requêtes de volumes persistants, de l'utilisation des requêtes de volumes persistants dans un pod et du redimensionnement des requêtes de volumes persistants. Nous avons abordé de nombreux concepts. Dans la prochaine vidéo, nous ferons une démonstration pratique de l'utilisation des volumes persistants dans notre cluster Kubernetes.

C'est tout pour cette leçon. Je vous verrai dans la prochaine.


# Reférence

https://kubernetes.io/docs/concepts/storage/persistent-volumes/

https://kubernetes.io/docs/concepts/storage/dynamic-provisioning/